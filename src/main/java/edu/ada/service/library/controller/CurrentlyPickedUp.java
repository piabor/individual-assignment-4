package edu.ada.service.library.controller;

import org.springframework.http.ResponseEntity;

public interface CurrentlyPickedUp {

    ResponseEntity myCurrentBooks(String token);

}
