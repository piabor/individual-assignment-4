package edu.ada.service.library.controller;

import org.springframework.http.ResponseEntity;

public interface ListBooks {

    ResponseEntity listPage();

}
