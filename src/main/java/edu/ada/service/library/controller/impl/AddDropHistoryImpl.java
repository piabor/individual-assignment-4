package edu.ada.service.library.controller.impl;

import edu.ada.service.library.controller.AddDropHistory;
import edu.ada.service.library.model.dto.AddDropHistoryModel;
import edu.ada.service.library.model.dto.UserModel;
import edu.ada.service.library.service.LibraryService;
import edu.ada.service.library.service.impl.JwtServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AddDropHistoryImpl implements AddDropHistory {

    @Autowired
    private JwtServiceImpl jwtService;

    @Autowired
    private LibraryService libraryService;

    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSImpl.class);

    @Override
    @RequestMapping(value = "/history", method = RequestMethod.GET)
    public ResponseEntity userHistory(
            @RequestHeader(value = "token") String token
    ) {
        try {
            String currentUserEmail = jwtService.extractUsername(token);
            UserModel currentUser = libraryService.getUserModelByEmail(currentUserEmail);
            List<AddDropHistoryModel> userHistory = libraryService.getUserHistory(currentUser);
            if(userHistory.size()>0) return ResponseEntity.ok(userHistory);
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return ResponseEntity.notFound().build();
    }
}
