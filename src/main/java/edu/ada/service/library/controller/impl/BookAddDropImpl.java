package edu.ada.service.library.controller.impl;

import edu.ada.service.library.controller.BookAddDrop;
import edu.ada.service.library.model.dto.BookAddDropModel;
import edu.ada.service.library.model.dto.BookModel;
import edu.ada.service.library.model.dto.UserModel;
import edu.ada.service.library.service.LibraryService;
import edu.ada.service.library.service.impl.JwtServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookAddDropImpl implements BookAddDrop {

    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSImpl.class);

    @Autowired
    private JwtServiceImpl jwtService;

    @Autowired
    private LibraryService libraryService;

    @Override
    @RequestMapping(value = "/add-drop", method = RequestMethod.GET)
    public ResponseEntity addDropBooks(
            @RequestHeader(value = "add", defaultValue = "3mpty4dd") String add,
            @RequestHeader(value = "drop", defaultValue = "3mptyDr0p") String drop,
            @RequestHeader(value = "token") String token
    ) {
        try {
            String currentUserEmail = jwtService.extractUsername(token);
            UserModel currentUser = libraryService.getUserModelByEmail(currentUserEmail);
            if (!add.equals("3mpty4dd") && drop.equals("3mptyDr0p")) {
                long addBookId = Long.parseLong(add);
                BookModel addedBook = libraryService.findBookById(addBookId);
                if (addedBook != null) {
                    if (addedBook.getAvailability().equals("Available")) {
                        libraryService.addBook(currentUser, addedBook);
                        log.info(currentUser.getEmail() + " picked up a book");
                        return ResponseEntity.ok("You have successfully picked up the book '" + addedBook.getTitle() + "'.");
                    } else if (libraryService.userAlreadyPickedThisBook(currentUser, addedBook)) {
                        log.warn("The user already picked up the book");
                        return ResponseEntity.ok("You have already picked up this book!");
                    } else {
                        UserModel currentBookUser = libraryService.findOwnerOfBookByBookId(addBookId);
                        log.warn("Unavailable book is requested by a user");
                        return ResponseEntity.ok("Book is unavailable and currently picked up by " + currentBookUser.getFirstName() + " " + currentBookUser.getLastName() + ".");
                    }

                } else {
                    log.error("Book with the given id is not found");
                    return ResponseEntity.notFound().build();
                }
            } else if (!drop.equals("3mptyDr0p") && add.equals("3mpty4dd")) {
                Long dropBookId = Long.parseLong(drop);
                BookAddDropModel droppingBook = libraryService.findDroppingBook(currentUser, dropBookId);
                if (droppingBook != null) {
                    libraryService.dropBook(currentUser, droppingBook);
                    log.info("A user dropped off a book");
                    return ResponseEntity.ok("You have successfully dropped off the book.");
                } else {
                    log.warn("A user tries to drop a book they did not pick up");
                    return ResponseEntity.notFound().build();
                }
            } else if (!add.equals("3mpty4dd") && !drop.equals("3mptyDr0p")) {
                log.warn("Add/drop parameters cannot be used together");
                return ResponseEntity.badRequest().build();
            }

            log.warn("Add/Drop parameter not given");
            return ResponseEntity.badRequest().build();
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
