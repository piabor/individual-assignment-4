package edu.ada.service.library.controller.impl;

import edu.ada.service.library.controller.ListBooks;
import edu.ada.service.library.model.dto.BookModelNoComment;
import edu.ada.service.library.service.LibraryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ListBooksImpl implements ListBooks {

    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSImpl.class);

    @Autowired
    private LibraryService libraryService;

    @Override
    @RequestMapping({"/list-books"})
    public ResponseEntity listPage() {
        try {
            List<BookModelNoComment> bookModelList = libraryService.getAllBooks();
            log.info("All books are listed");
            return ResponseEntity.ok(bookModelList);
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
