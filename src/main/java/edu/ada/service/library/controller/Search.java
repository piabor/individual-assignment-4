package edu.ada.service.library.controller;

import org.springframework.http.ResponseEntity;

public interface Search {

    ResponseEntity searchBooks(String title, String author, String genre);

}
