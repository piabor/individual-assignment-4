package edu.ada.service.library.controller.impl;

import edu.ada.service.library.controller.Comment;
import edu.ada.service.library.model.dto.BookModel;
import edu.ada.service.library.model.dto.UserModel;
import edu.ada.service.library.service.CommentService;
import edu.ada.service.library.service.LibraryService;
import edu.ada.service.library.service.impl.JwtServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CommentImpl implements Comment {

    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSImpl.class);

    @Autowired
    private JwtServiceImpl jwtService;

    @Autowired
    private LibraryService libraryService;

    @Autowired
    private CommentService commentService;

    @Override
    @GetMapping("/{id}")
    public ResponseEntity getBookByID(@PathVariable("id") long bookId) {
        try {
            BookModel book = libraryService.findBookById(bookId);
            if (book != null) return ResponseEntity.ok(book);
            else log.warn("Unavailable book is requested by a user");
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return ResponseEntity.notFound().build();
    }

    @Override
    @PostMapping("/{id}/comment")
    public ResponseEntity addComment(@PathVariable("id") long bookId,
                                     @RequestHeader(value = "comment-id", defaultValue = "3mptyC0mm3nt1d") String commentId,
                                     @RequestHeader(value = "content", defaultValue = "3mptyC0mm3ntC0nt3nt") String commentContent,
                                     @RequestHeader(value = "token") String token) {
        try {
            BookModel book = libraryService.findBookById(bookId);
            if (book != null) {
                String currentUserEmail = jwtService.extractUsername(token);
                UserModel currentUser = libraryService.getUserModelByEmail(currentUserEmail);
                if (!commentContent.equals("3mptyC0mm3ntC0nt3nt") && commentId.equals("3mptyC0mm3nt1d")) {
                    commentService.addComment(bookId, commentContent, currentUser);
                    log.info(currentUser.getFirstName() + " " + currentUser.getLastName() + " has posted a comment to a book.");
                    return ResponseEntity.ok("Comment is posted.");
                } else if (!commentContent.equals("3mptyC0mm3ntC0nt3nt") && !commentId.equals("3mptyC0mm3nt1d")) {
                    if (commentService.addReply(bookId, commentId, commentContent, currentUser)) {
                        log.info(currentUser.getFirstName() + " " + currentUser.getLastName() + " has replied to a comment.");
                        return ResponseEntity.ok("Reply is posted.");
                    }
                } else return ResponseEntity.badRequest().build();
            }
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return ResponseEntity.notFound().build();
    }

    @Override
    @DeleteMapping("/{id}/delete-comment")
    public ResponseEntity deleteComment(@PathVariable("id") long id,
                                     @RequestHeader(value = "comment-id") String commentId,
                                     @RequestHeader(value = "token") String token) {
        try {
            BookModel book = libraryService.findBookById(id);
            if (book != null) {
                String bookId = Long.toString(id);
                String currentUserEmail = jwtService.extractUsername(token);
                UserModel currentUser = libraryService.getUserModelByEmail(currentUserEmail);
                if(commentService.removeComment(commentId, currentUser, bookId)) {
                    log.info(currentUser.getFirstName() + " " + currentUser.getLastName() + " has deleted a comment and its sub-comments.");
                    return ResponseEntity.ok("The comment and the sub-comments are deleted.");
                } else{
                    return  ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                }
            }
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return ResponseEntity.notFound().build();
    }
}
