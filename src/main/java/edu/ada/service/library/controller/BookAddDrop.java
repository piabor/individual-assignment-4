package edu.ada.service.library.controller;

import org.springframework.http.ResponseEntity;

public interface BookAddDrop {

    ResponseEntity addDropBooks(String add, String drop, String token);

}
