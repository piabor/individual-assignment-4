package edu.ada.service.library.controller;

import edu.ada.service.library.model.dto.UserModel;
import org.springframework.http.ResponseEntity;

public interface AuthenticationWS {

    ResponseEntity createToken(String email, String password);

    ResponseEntity register(UserModel formData);

}
