package edu.ada.service.library.controller.impl;

import edu.ada.service.library.controller.CurrentlyPickedUp;
import edu.ada.service.library.model.dto.BookAddDropModel;
import edu.ada.service.library.model.dto.UserModel;
import edu.ada.service.library.model.entity.BookEntity;
import edu.ada.service.library.service.LibraryService;
import edu.ada.service.library.service.impl.JwtServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CurrentlyPickedUpImpl implements CurrentlyPickedUp {

    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSImpl.class);

    @Autowired
    JwtServiceImpl jwtService;

    @Autowired
    private LibraryService libraryService;

    @Override
    @RequestMapping(value = "/mybooks", method = RequestMethod.GET)
    public ResponseEntity myCurrentBooks(
            @RequestHeader(value = "token") String token
    ) {
        try {
            String currentUserEmail = jwtService.extractUsername(token);
            UserModel currentUser = libraryService.getUserModelByEmail(currentUserEmail);
            List<BookEntity> myBookList = new ArrayList<>();
            List<BookAddDropModel> currentBooks = libraryService.findCurrentlyPickedBooks(currentUser);
            if (currentBooks != null) {
                for (BookAddDropModel currentBook : currentBooks) {
                    myBookList.add(new BookEntity(libraryService.findBookById(currentBook.getBookId())));
                }
                return ResponseEntity.ok(myBookList);
            } else {
                ResponseEntity.badRequest().build();
                log.warn("User has not picked up any book");
            }
        } catch (Exception e){
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
