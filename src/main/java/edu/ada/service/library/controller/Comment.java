package edu.ada.service.library.controller;

import org.springframework.http.ResponseEntity;

public interface Comment {
    ResponseEntity getBookByID(long bookId);
    ResponseEntity addComment(long bookId, String commentId, String commentContent, String token);
    ResponseEntity deleteComment(long bookId, String commentId, String token);
}
