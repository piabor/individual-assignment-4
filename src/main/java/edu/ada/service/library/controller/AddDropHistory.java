package edu.ada.service.library.controller;

import org.springframework.http.ResponseEntity;

public interface AddDropHistory {

    ResponseEntity userHistory(String token);

}
