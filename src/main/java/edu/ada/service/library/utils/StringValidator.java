package edu.ada.service.library.utils;

import org.apache.commons.validator.routines.EmailValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringValidator {
    public static boolean isValidEmail(String email) {
        EmailValidator validator = EmailValidator.getInstance();
        if(validator.isValid(email)){
            return true;
        } else{
            System.out.println("Email is not valid!");
            return false;
        }
    }

    public static boolean isValidName(String name){
        String regex = "(?i)(^[a-z])((?![ .,'-]$)[a-z .,'-]){1,24}$";
        if(name != null) {
            return name.matches(regex);
        }
        return false;
    }

    public static boolean isValidPassword(String password) {

        String regex = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=\\S+$).{8,20}$";

        Pattern p = Pattern.compile(regex);
        if (password == null) {
            System.out.println("A password should be between 8 and 20 characters long and include at least one lower case, one upper case letter, and one number");
            return false;
        }
        Matcher m = p.matcher(password);
        if (m.matches()){
            return true;
        } else{
            System.out.println("A password should be between 8 and 20 characters long and include at least one lower case, one upper case letter, and one number");
            return false;
        }
    }
}