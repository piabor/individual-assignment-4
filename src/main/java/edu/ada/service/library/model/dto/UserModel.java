package edu.ada.service.library.model.dto;

import edu.ada.service.library.model.entity.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserModel implements Serializable {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String birthDay;
    private String password;

    public UserModel(UserEntity userEntity) {
        this.id = userEntity.getId();
        this.firstName = userEntity.getFirstName();
        this.lastName = userEntity.getLastName();
        this.email = userEntity.getEmail();
        this.birthDay = userEntity.getBirthDay();
    }
}
