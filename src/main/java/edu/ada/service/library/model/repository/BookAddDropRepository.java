package edu.ada.service.library.model.repository;

import edu.ada.service.library.model.entity.BookAddDropEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface BookAddDropRepository extends CrudRepository<BookAddDropEntity, Long> {
    BookAddDropEntity findFirstByUserIdAndBookId(Long userId, Long bookId);
    BookAddDropEntity findFirstByBookId(Long bookId);
    List<BookAddDropEntity> findAllByUserId(Long userId);
}
