package edu.ada.service.library.model.entity;

import edu.ada.service.library.model.dto.UserModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

@NoArgsConstructor
@Data
@Entity
@Table(name = "users",
        uniqueConstraints = {
        @UniqueConstraint(name= "student_email_unique", columnNames = "email")
})
public class UserEntity {

    @Id
    @SequenceGenerator(
            name = "user_sequence",
            sequenceName = "user_sequence",
            allocationSize = 1,
            initialValue = 1001
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "user_sequence"
    )
    @Column(
            name= "id",
            updatable = false
    )
    private Long id;

    private String firstName;
    private String lastName;
    private String birthDay;
    private String email;
    private String password;

    public UserEntity(UserModel userModel) {
        this.firstName = userModel.getFirstName();
        this.lastName = userModel.getLastName();
        this.birthDay = userModel.getBirthDay();
        this.email = userModel.getEmail();
        this.password = userModel.getPassword();
    }
}
