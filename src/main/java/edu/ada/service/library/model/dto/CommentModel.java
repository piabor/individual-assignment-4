package edu.ada.service.library.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import edu.ada.service.library.model.entity.CommentEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CommentModel implements Serializable {

    private String commentId;

    @JsonIgnore
    private String bookExtId;

    @JsonIgnore
    private String authorUserId;

    private String commentAuthor;

    private String commentContent;

    @JsonIgnore
    private String hasReplies;

    @JsonIgnore
    private String isParentComment;

    @JsonIgnore
    private String repliedCommentId;

    private List<CommentModel> replies;

    public CommentModel(CommentEntity commentEntity) {
        this.commentId = commentEntity.getId();
        this.bookExtId = commentEntity.getBookExtId();
        this.authorUserId = commentEntity.getAuthorUserId();
        this.commentAuthor = commentEntity.getCommentAuthor();
        this.commentContent = commentEntity.getCommentContent();
        this.hasReplies = commentEntity.getHasReplies();
        this.isParentComment = commentEntity.getIsParentComment();
        this.repliedCommentId = commentEntity.getRepliedCommentId();
    }

}
