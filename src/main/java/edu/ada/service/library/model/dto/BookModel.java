package edu.ada.service.library.model.dto;


import edu.ada.service.library.model.entity.BookEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BookModel implements Serializable {

    private Long id;
    private String title;
    private String author;
    private String description;
    private Date publishDate;
    private String genre;
    private String availability;
    private List<CommentModel> comments;

    public BookModel(BookEntity bookEntity) {
        this.id = bookEntity.getId();
        this.title = bookEntity.getTitle();
        this.author = bookEntity.getAuthor();
        this.description = bookEntity.getDescription();
        this.publishDate = bookEntity.getPublishDate();
        this.genre = bookEntity.getGenre();
        this.availability = bookEntity.getAvailability();
    }

    public void setComments(List<CommentModel> comments) {
        this.comments = comments;
    }
}
