package edu.ada.service.library.model.repository;

import edu.ada.service.library.model.entity.AddDropHistoryEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddDropHistoryRepository extends CrudRepository<AddDropHistoryEntity, Long> {
    List<AddDropHistoryEntity> findByOrderByRecordDateDesc();
}
