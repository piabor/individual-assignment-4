package edu.ada.service.library.model.entity;

import edu.ada.service.library.model.dto.CommentModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@Data
@Entity
@Document(collection = "comments")
public class CommentEntity {
    @Id
    private String id;

    private String bookExtId;

    private String authorUserId;

    private String commentAuthor;

    private String commentContent;

    private String hasReplies;

    private String isParentComment;

    private String repliedCommentId;

    @OneToMany
    private List<CommentEntity> replies;

    public CommentEntity(CommentModel commentModel) {
        this.bookExtId = commentModel.getBookExtId();
        this.authorUserId = commentModel.getAuthorUserId();
        this.commentAuthor = commentModel.getCommentAuthor();
        this.commentContent = commentModel.getCommentContent();
        this.hasReplies = commentModel.getHasReplies();
        this.isParentComment = commentModel.getIsParentComment();
        this.repliedCommentId = commentModel.getRepliedCommentId();
    }
}
