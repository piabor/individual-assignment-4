package edu.ada.service.library.model.entity;

import edu.ada.service.library.model.dto.BookAddDropModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity
@Table(name = "takenbooks", uniqueConstraints={
        @UniqueConstraint(columnNames = {"userId", "bookId"})
})
public class BookAddDropEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long userId;
    private Long bookId;

    public BookAddDropEntity(BookAddDropModel bookAddDropModel) {
        this.id = bookAddDropModel.getId();
        this.userId = bookAddDropModel.getUserId();
        this.bookId = bookAddDropModel.getBookId();
    }
}
