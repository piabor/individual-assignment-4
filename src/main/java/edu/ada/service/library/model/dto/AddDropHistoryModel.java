package edu.ada.service.library.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import edu.ada.service.library.model.entity.AddDropHistoryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AddDropHistoryModel implements Serializable {
    @JsonIgnore
    private Long userId;
    private Long bookId;
    private String addDropStatus;
    private String bookTitle;
    private Date recordDate;

    public AddDropHistoryModel(AddDropHistoryEntity addDropHistoryEntity) {
        this.userId = addDropHistoryEntity.getUserId();
        this.bookId = addDropHistoryEntity.getBookId();
        this.addDropStatus = addDropHistoryEntity.getAddDropStatus();
        this.bookTitle = addDropHistoryEntity.getBookTitle();
        this.recordDate = addDropHistoryEntity.getRecordDate();
    }
}
