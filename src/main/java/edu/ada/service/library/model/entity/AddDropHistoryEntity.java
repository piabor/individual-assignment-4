package edu.ada.service.library.model.entity;

import edu.ada.service.library.model.dto.AddDropHistoryModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Data
@Entity
@Table(name = "userbookhistory")
public class AddDropHistoryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long userId;
    private Long bookId;
    private String addDropStatus;
    private String bookTitle;
    private Date recordDate;

    public AddDropHistoryEntity(AddDropHistoryModel addDropHistoryModel) {
        this.userId = addDropHistoryModel.getUserId();
        this.bookId = addDropHistoryModel.getBookId();
        this.addDropStatus = addDropHistoryModel.getAddDropStatus();
        this.bookTitle = addDropHistoryModel.getBookTitle();
        this.recordDate = addDropHistoryModel.getRecordDate();
    }

}
