package edu.ada.service.library.model.dto;

import edu.ada.service.library.model.entity.BookAddDropEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BookAddDropModel implements Serializable {

    private Long id;
    private Long userId;
    private Long bookId;

    public BookAddDropModel(BookAddDropEntity bookAddDropEntity) {
        this.id = bookAddDropEntity.getId();
        this.userId = bookAddDropEntity.getUserId();
        this.bookId = bookAddDropEntity.getBookId();
    }
}
