package edu.ada.service.library.service;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;

public interface SecurityService{
    AuthenticationManager authenticationManagerBean() throws Exception;
    PasswordEncoder passwordEncoder();
}
