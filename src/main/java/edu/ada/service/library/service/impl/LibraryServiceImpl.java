package edu.ada.service.library.service.impl;

import edu.ada.service.library.model.dto.*;
import edu.ada.service.library.model.entity.*;
import edu.ada.service.library.model.repository.AddDropHistoryRepository;
import edu.ada.service.library.model.repository.BookAddDropRepository;
import edu.ada.service.library.model.repository.BookRepository;
import edu.ada.service.library.model.repository.UserRepository;
import edu.ada.service.library.service.CommentService;
import edu.ada.service.library.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class LibraryServiceImpl implements LibraryService {

    @Autowired
    private AddDropHistoryRepository addDropHistoryRepository;

    @Autowired
    private UserRepository userRepository;


    @Autowired
    private BookAddDropRepository bookAddDropRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private CommentService commentService;

    @Override
    public UserModel getUserModelByEmail(String email){
        return new UserModel(userRepository.findFirstByEmail(email));
    }

    @Override
    public List<AddDropHistoryModel> getUserHistory(UserModel currentUser){
        List<AddDropHistoryEntity> historyEntityList = addDropHistoryRepository.findByOrderByRecordDateDesc();
        for (int counter = historyEntityList.size() - 1; counter >= 0; counter--) {
            if (!historyEntityList.get(counter).getUserId().equals(currentUser.getId())) {
                historyEntityList.remove(counter);
            }
        }
        List<AddDropHistoryModel> history = new ArrayList<>();
        historyEntityList.forEach((historyEntity -> history.add(new AddDropHistoryModel(historyEntity))));
        return history;
    }

    @Override
    public BookModel findBookById(Long id){
        BookEntity book = bookRepository.findFirstById(id);
        if(book != null) {
            BookModel bookModel = new BookModel(book);
            bookModel.setComments(commentService.getCommentByBookExtId(id));
            return bookModel;
        }
        return null;
    }

    @Override
    public void addBook(UserModel user, BookModel book){
        bookRepository.changeAvailability("Unavailable! Taken by: " + user.getFirstName() + " " + user.getLastName(), book.getId());
        bookAddDropRepository.save(new BookAddDropEntity(new BookAddDropModel(
                null,
                user.getId(),
                book.getId()
        )));
        addDropHistoryRepository.save(new AddDropHistoryEntity(new AddDropHistoryModel(
                user.getId(),
                book.getId(),
                "Picked up",
                book.getTitle(),
                new Date()
        )));
    }

    @Override
    public void dropBook(UserModel user, BookAddDropModel droppingBook){
        BookAddDropEntity bookAddDropEntity = new BookAddDropEntity(droppingBook);
        bookRepository.changeAvailability("Available", bookAddDropEntity.getBookId());
        bookAddDropRepository.delete(bookAddDropEntity);

        addDropHistoryRepository.save(new AddDropHistoryEntity(new AddDropHistoryModel(
                user.getId(),
                droppingBook.getBookId(),
                "Dropped off",
                bookRepository.findFirstById(droppingBook.getBookId()).getTitle(),
                new Date()
        )));
    }

    @Override
    public boolean userAlreadyPickedThisBook(UserModel user, BookModel book){
        return bookAddDropRepository.findFirstByBookId(book.getId()).getUserId().equals(user.getId());
    }

    @Override
    public UserModel findOwnerOfBookByBookId(long id){
        return new UserModel(userRepository.findFirstById(bookAddDropRepository.findFirstByBookId(id).getUserId()));
    }

    @Override
    public BookAddDropModel findDroppingBook(UserModel user, Long id){
        BookAddDropEntity bookAddDropEntity = bookAddDropRepository.findFirstByUserIdAndBookId(user.getId(), id);
        if (bookAddDropEntity != null) return new BookAddDropModel(bookAddDropEntity);
        return null;
    }

    @Override
    public List<BookAddDropModel> findCurrentlyPickedBooks(UserModel user){
        List<BookAddDropEntity> bookAddDropEntityList = bookAddDropRepository.findAllByUserId(user.getId());
        List<BookAddDropModel> bookAddDropModels = new ArrayList<>(1);
        if(bookAddDropEntityList != null) {
            bookAddDropEntityList.forEach((addDropEntity -> bookAddDropModels.add(new BookAddDropModel(addDropEntity))));
        }
        return bookAddDropModels;
    }

    @Override
    public List<BookModelNoComment> getAllBooks(){
        Iterable<BookEntity> bookEntityList = bookRepository.findByOrderById();
        List<BookModelNoComment> books = new ArrayList<>();
        bookEntityList.forEach((bookEntity -> books.add(new BookModelNoComment(bookEntity))));
        return books;
    }

}