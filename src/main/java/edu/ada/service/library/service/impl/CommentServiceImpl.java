package edu.ada.service.library.service.impl;

import edu.ada.service.library.model.dto.CommentModel;
import edu.ada.service.library.model.dto.UserModel;
import edu.ada.service.library.model.entity.CommentEntity;
import edu.ada.service.library.model.repository.CommentRepository;
import edu.ada.service.library.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public List<CommentModel> getCommentByBookExtId(long id) {

        String bookId = Long.toString(id);
        List<CommentModel> comments = new ArrayList<>(1);
        Optional<List<CommentEntity>> commentEntityList = commentRepository.findAllByBookExtIdAndIsParentComment(bookId, "yes");
        if(commentEntityList.isEmpty()) return new ArrayList<>(1);
        else {
            commentEntityList.get().forEach((commentEntity -> {
                comments.add(new CommentModel(commentEntity));
                comments.get(comments.size()-1).setReplies(getCommentReplies(comments.get(comments.size()-1)));
            }));

        }
        return comments;
    }

    private List<CommentModel> getCommentReplies(CommentModel commentModel){
        List<CommentModel> replies = new ArrayList<>(1);
        Optional<List<CommentEntity>> repliesEntityList = commentRepository.findAllByRepliedCommentId(commentModel.getCommentId());
        if(repliesEntityList.isPresent()){
            repliesEntityList.get().forEach((currentCommentEntity -> {
                replies.add(new CommentModel(currentCommentEntity));
                replies.get(replies.size()-1).setReplies(getCommentReplies(replies.get(replies.size()-1)));
            }));
            return replies;
        }
        return null;
    }

    @Override
    public void addComment(long id, String content, UserModel user) {
        String bookId = Long.toString(id);
        commentRepository.save(new CommentEntity(new CommentModel(
                null,
                bookId,
                Long.toString(user.getId()),
                user.getFirstName() + " " + user.getLastName(),
                content,
                "no",
                "yes",
                null,
                null
        )));
    }

    @Override
    public boolean addReply(Long id, String commentId, String content, UserModel user){
        String bookId = Long.toString(id);
        CommentEntity replyingComment = commentRepository.findFirstById(commentId);
        if(replyingComment != null) {
            replyingComment.setHasReplies("yes");
            commentRepository.save(replyingComment);
            commentRepository.save(new CommentEntity(new CommentModel(
                    null,
                    bookId,
                    Long.toString(user.getId()),
                    user.getFirstName() + " " + user.getLastName(),
                    content,
                    "no",
                    "no",
                    commentId,
                    null
            )));
            return true;
        }
        return false;
    }

    @Override
    public boolean removeComment(String commentId, UserModel user, String bookId){
        CommentEntity deletingComment = commentRepository.findFirstByIdAndAuthorUserIdAndBookExtId(commentId, Long.toString(user.getId()), bookId);
        if(deletingComment != null) {
            deleteComment(deletingComment);
            Optional<List<CommentEntity>> repliesEntityList = commentRepository.findAllByRepliedCommentId(deletingComment.getRepliedCommentId());
            if(repliesEntityList.isEmpty()) {
                CommentEntity parentComment = commentRepository.findFirstById(deletingComment.getRepliedCommentId());
                parentComment.setHasReplies("no");
                commentRepository.save(parentComment);
            }
            return true;
        }
        return false;
    }

    private void deleteComment(CommentEntity commentEntity){
        Optional<List<CommentEntity>> repliesEntityList = commentRepository.findAllByRepliedCommentId(commentEntity.getId());
        repliesEntityList.ifPresent(commentEntities -> commentEntities.forEach((this::deleteComment)));
        commentRepository.deleteById(commentEntity.getId());
    }
}
