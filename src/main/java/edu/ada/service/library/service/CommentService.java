package edu.ada.service.library.service;

import edu.ada.service.library.model.dto.CommentModel;
import edu.ada.service.library.model.dto.UserModel;

import java.util.List;

public interface CommentService {

    List<CommentModel> getCommentByBookExtId(long id);
    void addComment(long id, String content, UserModel user);
    boolean addReply(Long id, String commentId, String content, UserModel user);
    boolean removeComment(String commentId, UserModel user, String bookId);

}
