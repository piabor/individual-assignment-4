package edu.ada.service.library.service.impl;

import edu.ada.service.library.model.dto.UserModel;
import edu.ada.service.library.model.entity.UserEntity;
import edu.ada.service.library.model.repository.UserRepository;
import edu.ada.service.library.service.AuthenticationService;
import edu.ada.service.library.utils.PasswordEncryption;
import edu.ada.service.library.utils.StringValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class AuthenticationServiceImpl implements UserDetailsService, AuthenticationService {

    protected static final Logger log = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public int registration(UserModel userModel) {
        try {
            if(userRepository.findFirstByEmail(userModel.getEmail()) == null) {
                if (StringValidator.isValidEmail(userModel.getEmail())) {
                    if (StringValidator.isValidPassword(userModel.getPassword())) {
                        if (StringValidator.isValidName(userModel.getFirstName()) && StringValidator.isValidName(userModel.getLastName())) {
                            userModel.setPassword(PasswordEncryption.hashPassword(userModel.getPassword()));
                            userRepository.save(new UserEntity(userModel));
                        } else {
                            return -3;
                        }
                    } else {
                        return -2;
                    }
                    return 1;
                } else {
                    return -1;
                }
            } else{
                return -4;
            }
        } catch (Exception e){
            log.error(e.getMessage());
            return 0;
        }
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity user;
        user = userRepository.findFirstByEmail(email);
        if (user != null) return new User(user.getEmail(), user.getPassword(), new ArrayList<>());
        return null;
    }
}
