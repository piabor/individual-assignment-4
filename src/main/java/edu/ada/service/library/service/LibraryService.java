package edu.ada.service.library.service;

import edu.ada.service.library.model.dto.*;

import java.util.List;

public interface LibraryService {
    UserModel getUserModelByEmail(String email);
    List<AddDropHistoryModel> getUserHistory(UserModel currentUser);
    BookModel findBookById(Long id);
    void addBook(UserModel user, BookModel book);
    void dropBook(UserModel user, BookAddDropModel droppingBook);
    boolean userAlreadyPickedThisBook(UserModel user, BookModel book);
    UserModel findOwnerOfBookByBookId(long id);
    BookAddDropModel findDroppingBook(UserModel user, Long id);
    List<BookAddDropModel> findCurrentlyPickedBooks(UserModel user);
    List<BookModelNoComment> getAllBooks();
}
