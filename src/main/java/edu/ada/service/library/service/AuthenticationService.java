package edu.ada.service.library.service;

import edu.ada.service.library.model.dto.UserModel;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface AuthenticationService {
    int registration(UserModel userModel);
    UserDetails loadUserByUsername(String email) throws UsernameNotFoundException;
}
